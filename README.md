# hemp-back

This project was generated with nodejs version 14.17.1 and the docker image mariadb:10.3.32  

## Run environment
1 - In a first shell, start the stack with the docker compose command: 
```sh
docker-compose up
```

2 - In a second shell, enter in the API container with :
```sh
cd api
docker-compose exec api bash
```

## Development server
In this container, 
Run `npm run start` for a dev server. Navigate to `http://localhost:8888/`. 


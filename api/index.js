
/************************************/
/*** Import des modules nécessaires */
const express = require("express")
const cors = require ('cors')
const checkTokenMiddleware = require('./jsonwebtoken/check')
/************************************/
/*** Import de la connexion à la BD */
let DB = require('./db.config')

/************************************/
/*** Initialisation de l'API */
const app = express()


app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true}))

/************************************/
/*** Import des modules de routage */

const user_router = require('./routes/users')
const userType_router = require('./routes/usersType')
const auth_router = require('./routes/auth')

/************************************/
/*** Mise en place du routage - Branchement */
app.get('/', (req,res) => res.send(`Tout fonctionne parfaitement. C'est super !!`))
app.use('/users', user_router)
app.use('/usersType', checkTokenMiddleware, userType_router)
app.use('/auth', auth_router)
app.get('*', (req, res) =>res.status(501).send(`La ressource n'existe pas !`))

/************************************/
/*** Démarrage du serveur avec test DB*/
DB.authenticate()
  .then( () => console.log('connection à la DB OK !'))
  .then( () => {
    app.listen(process.env.SERVER_PORT, () => {
      console.log(`listining on port ${process.env.SERVER_PORT}. Fantastique, le serveur est lancé !!!`)
    })
  }) 
  .catch(err => console.log('Database Error', err))




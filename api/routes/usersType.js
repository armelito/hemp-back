
/***********************************/
/** Import des modules nécessaires */
const express = require('express')
const userTypeCtrl = require('../controllers/userType')

/***********************************/
/** Récupération du routeur d'express */
let router = express.Router()

/***********************************/
/** Middleware GENERALISTE pour logger les dates de requête */
router.use(  (req, res, next) => {
    const event = new Date()
    console.log('USER TYPE Time:', event.toString(), 'type de protocole:',req.protocol)
    next()
})

/** GET ALL */
router.get('', userTypeCtrl.getAllUsersType)

/** GET ONE */
router.get('/:id', userTypeCtrl.getUserType) 

/** CREATE - PUT */
router.put('', userTypeCtrl.addUserType) 


/** UPDATE - PATCH */
router.patch('/:id', userTypeCtrl.updateUserType) 


/** POST - RESTAURE (donnée dans corbeille) */
router.post('/untrash/:id', userTypeCtrl.untrashUserType) 

/** SOFT DELETE - corbeille (récupérable) */
router.delete('/trash/:id', userTypeCtrl.trashUserType) 

/** DELETE  - définitivement */
router.delete('/:id', userTypeCtrl.deleteUserType) 


module.exports = router
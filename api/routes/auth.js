/***********************************/
/** Import des modules nécessaires */
const express = require('express')
const User = require('../models/user')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
/***********************************/
/** Récupération du routeur d'express */
let router = express.Router()


/***********************************/
/** Middleware GENERALISTE pour logger les dates de requête */
router.use(  (req, res, next) => {
    const event = new Date()
    console.log('AUTH Time:', event.toString())
    next()
})

/***********************************/
/** Routage de la resource Auth */
/** comme pas de PUT ni de PATCH, car ni création ni update, on utilise le POST (envoie de formulaire) */
/** next() = middleware uniquement sur une route spécifique */
router.post('/login', (req, res) => {
    const { pseudo, password } = req.body

    // Validation des données reçues
    if(!pseudo || !password) {
        return res.status(400).json({message: 'Bad pseudo or password'})
    }

    User.findOne({where: {pseudo: pseudo}, raw: true})
        .then(user => {
            // Vérification si l'user existe
            if (user === null) {
            return res.status(401).json({ message: 'This user does not exist !' })
        }

        // Vérification du mot de passe
        // Avec bcrypt et la méthode compare(), on compare le password de la request
        // avec le password contenu en BDD
        bcrypt.compare(password, user.password)
            .then(test => {
                if(!test){
                    return res.status(401).json({message: 'Wrong password'})
                }
                // Si pas d'erreur, génération du token
                // on utilise la méthode jwt.sign() qui reçoit en premier param le payload (charge utile de la request)
                //jwt.sign({payload}, secret phrase, duration)
                const token = jwt.sign({
                    id: user.id,
                    nom: user.nom,
                    prenom: user.prenom,
                    pseudo: user.pseudo
                }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_DURING })

                return res.json({access_token:token})


            })
            .catch(err => res.status(500).json({ message: 'Login process failed', error:err}))
        })
    .catch(err => res.status(500).json({ message:'Database error', error: err }))
})

// export du module de routage
module.exports = router

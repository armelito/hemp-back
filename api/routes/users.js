/***********************************/
/** Import des modules nécessaires */
const express = require('express')
const userCtrl =require('../controllers/user')

// const checkTokenMiddleware = require('../jsonwebtoken/check')
/***********************************/
/** Récupération du routeur d'express */
let router = express.Router()

/***********************************/
/** Middleware GENERALISTE pour logger les dates de requête */
router.use(  (req, res, next) => {
    const event = new Date()
    console.log('USER Time:', event.toString())
    next()
})

/** GET ALL */
/**apidoc
 * @api {get} / Récupération des utilisateurs
 * @apiDescription Retourne la liste de tous les utilisateurs.
 * @apiName GetUsers
 * @apiGroup User
 * @apiSuccess {User[]} users Tableau des users
 * @apiSuccessExample Exemple de réponse avec succès
 *     HTTP/1.1 200 OK
 {
    "data": [
        {
            "id": 1,
            "nom": "Dénéchaud",
            "prenom": "Armel",
            "pseudo": "armelito",
            "password": "$2b$10$LnZrfQm7F6rXWKbb/DmKoeTFzKg0SUXoljs66vCePRY4u4GnXUEwG",
            "createdAt": "2022-01-29T14:52:24.000Z",
            "updatedAt": "2022-01-29T14:52:24.000Z",
            "deletedAt": null
        },
        {
            "id": 2,
            "nom": "Twist",
            "prenom": "Olivier",
            "pseudo": "Oliver",
            "password": "$2b$10$3pmj4VI6n7MzVfZbXmNMAOYqm6SK6bGGjzQbTUiRMR3uz0.jNYMAy",
            "createdAt": "2022-02-08T17:36:00.000Z",
            "updatedAt": "2022-02-08T17:36:00.000Z",
            "deletedAt": null
        },
        {
            "id": 3,
            "nom": "Maupetit",
            "prenom": "Julien",
            "pseudo": "juliano40",
            "password": "$2b$10$s1cy.R3xmw.uJGfNJdAwpeWfheOOwv.k8iWvlcYGoDVDbsclA3wXm",
            "createdAt": "2022-02-11T19:12:23.000Z",
            "updatedAt": "2022-02-11T19:12:23.000Z",
            "deletedAt": null
        },
        {
            "id": 4,
            "nom": "Barré",
            "prenom": "Etienne",
            "pseudo": "daxeti40",
            "password": "$2b$10$XiCvkxvnccZRgsQp3Hxw8umdmHsxWUrykJc/D8C2WvI.21AjoIYdW",
            "createdAt": "2022-02-11T19:14:39.000Z",
            "updatedAt": "2022-02-11T19:14:39.000Z",
            "deletedAt": null
        },
        
    ]
}
 
 * @apiError (Erreur 500) {String} message Description du problème
 * @apiError (Erreur 500) {String} error Si statut HTTP 500, Valeur du paramètre error de la méthode catch
 * @apiErrorExample Exemple erreur 500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": 'Database error',
 *       "error": err
 *     }
 * @apiSampleRequest http://172.22.0.3:8888/users
 */
router.get('/', userCtrl.getAllUsers)

/** GET ONE */
/**apidoc

 * @api {get} /:id Récupération d'un utilisateur 
 * @apiDescription Retourne un utilisateur en particulier.
 * @apiName GetOneUser
 * @apiGroup User
 
 * @apiSuccess {User:id} utilisateur unique 
 * @apiSuccessExample Exemple de réponse de succès:
 *     HTTP/1.1 200 OK
 *     
 *  {
 *      "data": {
 *          "id": 2,
 *          "nom": "Twist",
 *          "prenom": "Olivier",
 *          "pseudo": "Oliver",
 *          "password": "$2b$10$3pmj4VI6n7MzVfZbXmNMAOYqm6SK6bGGjzQbTUiRMR3uz0.jNYMAy",
 *          "createdAt": "2022-02-08T17:36:00.000Z",
 *          "updatedAt": "2022-02-08T17:36:00.000Z",
 *          "deletedAt": null
 *       }
 *   } 
  
 * @apiError (Erreur 404) {String} message Description du problème
 * @apiError (Erreur 404){String} error Si statut HTTP 404, valeur du paramètre error if user = null
 * 
 * @apiErrorExample Exemple erreur 404:
 *     HTTP/1.1 404 Not Found
 *     {
 *        "message": "This user does not exist !"
 *      }
 * @apiError (Erreur 500) {String} message Description du problème
 * @apiError (Erreur 500) {String} error Si statut HTTP 500, valeur du paramètre err if erreur 500
 * @apiErrorExample Exemple erreur 500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Database error",
 *       "error": err
 *     }
 * @apiSampleRequest http://172.22.0.3:8888/users/:id
 * @apiParam {Number} id

 */
router.get('/:id', userCtrl.getUser)

    
/** CREATE - PUT */
/**apidoc
 
 * @api {put} 
 * @apiBody {String} nom         nom de l'utilisateur
 * @apiBody {String} prenom      prénom de l'utilisateur
 *
 * @apiBody {String} pseudo      pseudo unique choisi par l'utilisateur pour se logger
 * @apiBody {String} password    mot de passe choisi par l'utilisateur
 
 * @api {put} / Création d'un utilisateur 
 * @apiDescription Retourne un utilisateur nouvellement créé.
 * @apiName PutOneUser
 * @apiGroup User
 * @apiSuccess {User:id} id unique 
 * @apiSuccess {String} nom nom de l'utilisateur
 * @apiSuccess {Sring} prenom prenom de l'utilisateur
 * @apiSuccess {String} pseudo unique 
 * @apiSuccess {String} password mot de passe
 * @apiSuccess {Date} updatedAt date de mise à jour
 * @apiSuccess {Date} createdAt date de création
 * @apiSuccessExample Exemple de réponse de succès:
 *     HTTP/1.1 200 OK
 *     
 *  {
    "message": "User Created",
    "data": {
        "id": 9,
        "nom": "Bourget",
        "prenom": "Florian",
        "pseudo": "floriano",
        "password": "$2b$10$CPxFf0GRnpKPFQsFwICye.B5cqHZg95LZIktH4SPgwhqIr4V38F9G",
        "updatedAt": "2022-04-07T13:57:36.901Z",
        "createdAt": "2022-04-07T13:57:36.901Z"
    }
}
 * @apiError (Erreur 400) {String} message Description du problème
 * @apiError (Erreur 400){String} error Si statut HTTP 400, un des paramètres suivant est manquant: nom, prenom, pseudo, password
 * 
 * @apiErrorExample Exemple erreur 400:
 *     HTTP/1.1 400 Bad Request
 *     {
 *        "message": "Missing data !"
 *      }
 * @apiError (Erreur 409) {String} message Description du problème
 * @apiError (Erreur 409){String} error Si statut HTTP 409, un pseudo identique existe déjà en BDD
 * 
 * @apiErrorExample Exemple erreur 409:
 *     HTTP/1.1 409 Bad Request
 *     {
 *        "message": "The user ${nom} already exists !"
 *      }
 * @apiError (Erreur 500) {String} message Description du problème
 * @apiError (Erreur 500) {String} error Si statut HTTP 500, valeur du paramètre err if erreur 500
 * @apiErrorExample Exemple erreur 500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Database error",
 *       "error": err
 *     }
 * @apiSampleRequest http://172.22.0.3:8888/users
 */
router.put('', userCtrl.addUser)
    

/** UPDATE - PATCH */
/**apidoc
 
 * @api {patch} /:id Modification d'un utilisateur
 * @apiDescription Modifie/met à jour un utilisateur.
 * @apiBody {String} [nom]         nom de l'utilisateur
 * @apiBody {String} [prenom]      prénom de l'utilisateur
 *
 * @apiBody {String} pseudo      pseudo unique choisi par l'utilisateur pour se logger
 * @apiBody {String} [password]    mot de passe choisi par l'utilisateur

 * @apiName PatchOneUser
 * @apiGroup User
 * @apiSuccess {User:id} id id unique de l'utilisateur 
 * @apiSuccess {String} nom nom de l'utilisateur
 * @apiSuccess {Sring} prenom prénom de l'utilisateur
 * @apiSuccess {String} pseudo unique 
 * @apiSuccess {String} password mot de passe
 * @apiSuccess {Date} updatedAt date de mise à jour
 * @apiSuccess {Date} createdAt date de création
 * @apiSuccessExample Exemple de réponse de succès:
 *     HTTP/1.1 200 OK
 *     
 * {
    "message": "User Updated"
}
}
 * @apiError (Erreur 400) {String} message Description du problème
 * @apiError (Erreur 400){String} error Si statut HTTP 400, un des paramètres suivant est manquant: nom, prenom, pseudo, password
 * 
 * @apiErrorExample Exemple erreur 400:
 *     HTTP/1.1 400 Bad Request
 *     {
 *        "message": "Missing data !"
 *      }
 * @apiError (Erreur 409) {String} message Description du problème
 * @apiError (Erreur 409){String} error Si statut HTTP 409, un pseudo identique existe déjà en BDD
 * 
 * @apiErrorExample Exemple erreur 409:
 *     HTTP/1.1 409 Bad Request
 *     {
 *        "message": "The user ${nom} already exists !"
 *      }
 * @apiError (Erreur 500) {String} message Description du problème
 * @apiError (Erreur 500) {String} error Si statut HTTP 500, valeur du paramètre err if erreur 500
 * @apiErrorExample Exemple erreur 500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Database error",
 *       "error": err
 *     }
 * @apiSampleRequest http://172.22.0.3:8888/users/:id
 * @apiParam {Number} id id unique de l'utilisateur
 */
router.patch('/:id', userCtrl.updateUser)

/** POST - RESTAURE (donnée dans corbeille) */
router.post('/untrash/:id', userCtrl.untrashUser) 

/** SOFT DELETE - corbeille (récupérable) */
router.delete('/trash/:id', userCtrl.untrashUser) 

/** DELETE  - définitivement */
/**apidoc
 * @api {delete} /:id Suppression d'un utilisateur
 * @apiDescription Supprime définitivement un utilisateur.
 * @apiName DeleteUser
 * @apiGroup User
 * @apiSuccess {User:id} user utilisateur effacé
 * @apiSuccessExample Exemple de réponse avec succès
 *     HTTP/1.1 200 OK
 {
    "data": 
        {
            'User definitively deleted'
        }

 * @apiError (Erreur 400) {String} message Description du problème
 * @apiError (Erreur 400){String} error Si statut HTTP 400, le paramètre id est manquant
 * 
 * @apiErrorExample Exemple erreur 400:
 *     HTTP/1.1 400 Bad Request
 *     {
 *        "message": "Missing parameter !"
 *      }       
 
 * @apiError (Erreur 500) {String} message Description du problème
 * @apiError (Erreur 500) {String} error Si statut HTTP 500, valeur du paramètre error de la méthode catch
 * @apiErrorExample Exemple erreur 500:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": 'Database error',
 *       "error": err
 *     }
 * @apiSampleRequest http://172.22.0.3:8888/users/:id
 * @apiParam {Number} id id de l'utilisateur à supprimer
 */
router.delete('/:id', userCtrl.deleteUser) 

module.exports = router

/***********************************/
/** Import des modules nécessaires */

const req = require('express/lib/request')
const jwt = require('jsonwebtoken')

/***********************************/
/** Extraction du token */
const extractBearer = authorization => {
    if (typeof authorization !== 'string') {
        return false
    }

    // Si typeof = string alors Isolation du token
    const matches = authorization.match(/(bearer)\s+(\S+)/i)
    
    return matches && matches[2]
}


/** Vérification de la présence du token - donc middleware*/
const checkTokenMiddleware = (req, res, next) => {
    // 1 -Vérification de la présence de l'attribut "authorization" dans l'entête de la requête
    // 2 - Extraction du token
    const token = req.headers.authorization && extractBearer(req.headers.authorization)
    console.log('HEADERS:', req.headers)
    console.log('TOKEN:', token)

    if (!token) {
        return res.status(401).json({message: 'Token non présent, vous ne passerez pas !'})
    }

    // Si token présent, vérification de la validité du token
    // on utilise la méthode verify de jwt
    // elle reçoit le paramètre token, la phrase secrète, et une call back
    // qui contiendra soit une erreur soit le token décodé
    jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
        
        if (err) {
            // message si non valide ou mal constitué
            res.status(401).json({ message: 'Bad token' })
        }

        // si ok alors relache de la requête avec next()
        next()
    })
}

module.exports = checkTokenMiddleware
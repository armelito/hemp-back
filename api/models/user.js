/***************************************/
/*** Import des modules nécessaires */
/*** Chaque donnée étant typée dans un model, on appelle le DatatType de Sequelize */
/*** On appelle également ce qui permet de connecter le model à la base  */
const { DataTypes } = require('sequelize')
const DB = require('../db.config') 

/***************************************/
/*** Définition du model User */
/** Dans Sequelize, on définit un model avec la méthode Db.define() */
const User = DB.define('User',{
    id: {
        type: DataTypes.INTEGER(10),
        primaryKey: true,
        autoIncrement: true
    },
    nom: {
        type: DataTypes.STRING(100),
        defaultValue:'',
        allowNull: false
    },
    prenom: {
        type: DataTypes.STRING(100),
        defaultValue:'',
        allowNull: false
    },
    pseudo: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: true
    },
    password: {
        // Hachage du password en BDD en chaine de 64 caractère
        type: DataTypes.STRING(64), 
        // on impose une contrainte avec is:
        is: /^[0-9a-f]{64}$/i

    }
    
    // softDelete(on efface mais pas définitivement, like corbeille dans windows)
}, { paranoid: true })


User.associate = function(models) {
    User.hasOne(models.Profile, {
        foreignKey: 'user_id',
        as: 'userType',
    });

};


/***********************************************************************/
/** Synchronisation du modèle avec la BDD si besoin ( évite de tout synchroniser) */

User.sync()
// User.sync({force: true})
// User.sync({alter:true})

module.exports = User
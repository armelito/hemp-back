/***************************************/
/*** Import des modules nécessaires */
/*** Chaque donnée étant typée dans un model, on appelle le DatatType de Sequelize */
/*** On appelle également ce qui permet de connecter le model à la base  */
const { DataTypes } = require('sequelize')
const DB = require('../db.config') 

/***************************************/
/*** Définition du model User_type */
/** Dans Sequelize, on définit un model avec la méthode Db.define() */
const UserType = DB.define('UserType',{
    id: {
        type: DataTypes.INTEGER(10),
        primaryKey: true,
        autoIncrement: true
    },
    nom: {
        type: DataTypes.STRING(20),
        defaultValue:'',
        allowNull: false
    },
    // user_id: {
    //     type: DataTypes.INTEGER(10),
    //     foreignKey: true,
    //     allowNull: false
    // }
   
   // softDelete( on efface mais pas définitivement, like corbeille dans windows)
}, { paranoid: true })



/***********************************************************************/
/** Synchronisation du modèle si besoin ( évite de tout synchroniser) */

// UserType.sync()
// UserType.sync({force: true})
// UserType.sync({alteuserType
module.exports = UserType
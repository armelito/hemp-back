/*********************************************/
/***Import des modules nécessaires */
//require('dotenv').config();
/** les {} permettent d'importer uniquement ce qui nous interesse, ici Sequelize */
const { Sequelize } = require('sequelize')

/*********************************************/
/***Connexion à la base de données */
/*** Variable sequelize et nouvelle instance de Sequelize à laquelle on passe les paramètres de connexion */
let sequelize = new Sequelize(
    process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: 'mysql',
        logging:false
    }
)

/*********************************************/
/*** Synchronisation des modèles */
sequelize.sync(err => {
    console.log('Database Sync Error', err)
})

module.exports = sequelize
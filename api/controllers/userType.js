
/***********************************/
/** Import des modules nécessaires */
const UserType = require('../models/userType')
const bcrypt = require('bcrypt')

/***********************************/
/** Routage de la ressource (ressource=chemin) User */
/**  Dans le restful, on utilise put et non post* SAUF si on ne peut pas se servir du PUT et du PATH ex: restaure/


/** GET ALL et branchement du token middleware pour soumettre la route à l'identification du user */
exports.getAllUsersType = (req,res) => {
    UserType.findAll()
    // on reçoit une variable users qui contient un résultat en Json et qu'on envoie avec data
    .then(usersType => res.json({ data:usersType }))
    .catch( err => res.status(500).json({ message: 'Database error', error: err }))
}

/** GET ONE */
exports.getUserType = (req, res) => {
    let userTypeId = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    // Si pas userId, return d'un json(avec code 400 pour indiquer qu'il y a un problème) 
    // puis envoie en json d'un message indiquant qu'un paramètre est manquant.
    if(!userTypeId) {
        return res.json(400).json({ message: 'Missing Parameter'})
    }

    // Récupération de l'utilisateur (si pas de passage dans le if)
    // la méthode findOne de l'ORM Sequelize récupère le userId demandé 
    User.findOne({ where: {id: userId}, raw: true })
        .then(user => {
            if ((user === null)) {
                return res.status(404).json({ message: 'This user does not exist !'})

            }
            // utilisateur trouvé
            return res.json({ data: user})
        })
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
}

/** CREATE - PUT */
// Créer une ressource - (dans le restful, il sert aussi à la modification totale de la resource)
// rien dans l'url, (pas d'ID), tout dans le body
exports.addUserType = (req, res) => {
    const {nom} = req.body
    // on utilise body car c'est dans le corps de la request que vont se trouver les données = payload = charge utile de la request

    // Validation des données
    if( !nom || !user_id ) {
        return res.status(400).json({ message: 'Missing data !'})
    }

    // on recherche si le userType existe afin de ne pas le recréer
    // on teste donc l'existence d'un nom identique MAIS dans la BDD
    // raw:true permet de tester ce qu'on reçoit ( si oublie, pas de test possible)
    // le premier nom est celui qui se trouve en BDD, le second celui contenu dans le body de la request
    UserType.findOne({ where: { nom: nom }, raw: true})
        .then(userType => {
            // Vérification si le user type existe déjà
            if (userType !== null) {
                return res.status(409).json({ message: `The user type ${nom} already exists !` })
            }

            // Création de l'utilisateur
            UserType.create(req.body)
                .then(userType => res.json({message: 'User type Created', data:user}))
                .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
            })

        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
}


/** UPDATE - PATCH */
// Modifier une ressource - (dans le restful, il sert à la modification partielle de la ressource)
// Quelque chose dans l'url, (un ID), et dans le body

exports.updateUserType = (req, res) => {
    let userTypeId = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    if(!userTypeId) {
        return res.status(400).json({message: 'Missing parameter'})
    }
    // Recherche du UserType
    UserType.findOne({ where: {id: userTypeId}, raw: true})
        .then(userType => {
            // Vérifier si l'utilisateur existe
            if(userType === null) {
                return res.status(404).json({ message: 'This userType does not exist'})
            }

            // Mise à jour de l'utilisateur
            // on utilise req.body car c'est bien dans le corps de la request que figurent les params à mettre à jour 
            // {id: userTypeId} = id de la BDD correspond à l'ID reçu dans l'url
            UserType.update(req.body, { where: {id: userTypeId}})
                .then(userType => res.json({ message: 'UserType Updated'}))
                .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
        })
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
}

/** POST - RESTAURE (donnée dans corbeille) */
exports.untrashUserType = (req, res) => {
    let userTypeId = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    if(!userTypeId) {
        return res.status(400).json({message: 'Missing parameter'})
    }
    UserType.restore({ where: {id: userTypeId}})
        .then(() => res.status(204).json({}))
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
}

/** SOFT DELETE - corbeille (récupérable) */
exports.trashUserType = (req, res) => {
    let user_type_id = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    if (!user_type_id) {
        return res.status(400).json({ message: 'Missing parameter'})
    }
    // Suppression de l'utilisateur - force: true = définitivement
    // 204 - indique que la requête a réussi mais qu'aucun contenu n'est à afficher 
    UserType.destroy({ where: {id: user_type_id}})
        .then(() => res.status(204).json({}))
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
    
}

/** DELETE  - définitivement */
exports.deleteUserType = (req, res) => {
    let user_type_id = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    if (!user_type_id) {
        return res.status(400).json({ message: 'Missing parameter'})
    }
    
    // Suppression de l'utilisateur - force: true = définitivement
    // 204 - indique que la requête a réussi mais qu'aucun contenu n'est à afficher 
    UserType.destroy({ where: {id: user_type_id}, force: true})
        .then(() => res.status(204).json({}))
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
    
}



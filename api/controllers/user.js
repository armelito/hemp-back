/***********************************/
/** Import des modules nécessaires */
const User = require('../models/user')
const bcrypt = require('bcrypt')

/***********************************/
/** Routage de la ressource (ressource=chemin) User */
/**  Dans le restful, on utilise put et non post* SAUF si on ne peut pas se servir du PUT et du PATH ex: restaure/


/** GET ALL et branchement du token middleware pour soumettre la route à l'identification du user */
exports.getAllUsers = (req, res) => {
    User.findAll()
    // on reçoit une variable user qui contient un résultat en Json et qu'on envoie avec data
    .then(users => res.json({ data:users }))
    .catch( err => res.status(500).json({ message: 'Database error', error: err }))
}

/** GET ONE */
// Méthode déclarée asynchrone car elle va attendre une réponse de la BDD avant de poursuivre son travail
exports.getUser =  async (req, res) => {
    let userId = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    // Si pas userId, return d'un json(avec code 400 pour indiquer qu'il y a un problème) 
    // puis envoie en json d'un message indiquant qu'un paramètre est manquant.
    if(!userId) {
        return res.json(400).json({ message: 'Missing Parameter'})
    }

    try{
        // on stocke dans la variable user le résultat de la fonction User.findOne
    // Cette méthode se met en attente de la réponse de la BDD à l'aide du mot clé "await"
    // la méthode findOne de l'ORM Sequelize récupère le userId demandé
    let user = await User.findOne({ where: {id: userId}, raw: true })

    // test si le user existe
    if ((user === null)) {
        return res.status(404).json({ message: 'This user does not exist !'})
    }

    // renvoi le user trouvé
    return res.json({ data: user})

    }catch(err) {
        res.status(500).json({ message: 'Database Error', error: err})
    }

    

    // Récupération de l'utilisateur (si pas de passage dans le if)
    // User.findOne({ where: {id: userId}, raw: true })
    //     .then(user => {
    //         if ((user === null)) {
    //             return res.status(404).json({ message: 'This user does not exist !'})

    //         }
    //         // utilisateur trouvé
    //         return res.json({ data: user})
    //     })
    //     .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
}


/** CREATE - PUT */
// Créer une ressource - (dans le restful, il sert aussi à la modification totale de la resource)
// rien dans l'url, (pas d'ID), tout dans le body
exports.addUser = (req, res) => {
    const {nom, prenom, pseudo, password} = req.body
    // on utilise body car c'est dans le corps de la request que vont se trouver les données = payload = charge utile de la request

    // Validation des données
    if( !nom || !prenom || !pseudo || !password ) {
        return res.status(400).json({ message: 'Missing data !'})
    }

    // on recherche si l'utilisateur existe afin de ne pas le recréer
    // on teste donc l'existence d'un pseudo identique MAIS dans la BDD
    // raw:true permet de tester ce qu'on reçoit
    // le premier pseudo est celui qui se trouve en BDD, le second celui contenu dans la request
    User.findOne({ where: { pseudo: pseudo }, raw: true})
        .then(user => {
            // Vérification si l'utilisateur existe déjà
            if (user !== null) {
                return res.status(409).json({ message: `The user ${nom} already exists !` })
            }

            // Implémentation de bcrypt pour hachage password
            // Un password doit être crypté, haché et salé (cryptage , hachage, salage)
            bcrypt.hash(password, parseInt(process.env.BCRYPT_SALT_ROUND))
            .then(hash => {
                // remplacement du password en clair en password haché
                req.body.password = hash
                
                // Création de l'utilisateur
                User.create(req.body)
                    .then(user => res.json({message: 'User Created', data:user}))
                    .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
            })
            .catch(err => res.status(500).json({message: 'Hash Process Error', error:err}))

            
        })
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
}

/** UPDATE - PATCH */
// Modifier une ressource - (dans le restful, il sert à la modification partielle de la ressource)
// Quelque chose dans l'url, (un ID), et dans le body

exports.updateUser = (req, res) => {
    let userId = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    if(!userId) {
        return res.status(400).json({message: 'Missing parameter'})
    }
    // Recherche de l'utilisateur
    User.findOne({ where: {id: userId}, raw: true})
        .then(user => {
            // Vérifier si l'utilisateur existe
            if(user === null) {
                return res.status(404).json({ message: 'This user does not exist'})
            }

            // Mise à jour de l'utilisateur
            // on utilise req.body car c'est bien dans le corps de la request que figurent les params à mettre à jour 
            // {id: userId} = id de la BDD correspond à l'ID reçu dans l'url
            User.update(req.body, { where: {id: userId}})
                .then(user => res.json({ message: 'User Updated'}))
                .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
        })
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
}

/** POST - RESTAURE (donnée dans corbeille) */
exports.untrashUser = (req, res) => {
    let userId = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    if(!userId) {
        return res.status(400).json({message: 'Missing parameter'})
    }
    User.restore({ where: {id: userId}})
        .then(() => res.status(204).json({}))
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
}

/** SOFT DELETE - corbeille (récupérable) */
exports.trashUser = (req, res) => {
    let user_id = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    if (!user_id) {
        return res.status(400).json({ message: 'Missing parameter'})
    }
    // Suppression de l'utilisateur - force: true = définitivement
    // 204 - indique que la requête a réussi mais qu'aucun contenu n'est à afficher 
    User.destroy({ where: {id: user_id}})
        .then(() => res.status(204).json({}))
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
    
}

/** DELETE  - définitivement */
exports.deleteUser = (req, res) => {
    let user_id = parseInt(req.params.id)

    // Vérification si le champ id est présent et cohérent
    if (!user_id) {
        return res.status(400).json({ message: 'Missing parameter !'})
    }
    
    // Suppression de l'utilisateur - force: true = définitivement
    // 204 - indique que la requête a réussi mais qu'aucun contenu n'est à afficher 
    User.destroy({ where: {id: user_id}, force: true})
        .then(() => res.status(200).json({message: 'User definitively deleted'}))
        .catch(err => res.status(500).json({ message: 'Database Error', error: err}))
    
}

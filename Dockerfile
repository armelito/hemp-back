FROM node:16

RUN npm install -g nodemon

RUN npm install -g sequelize-cli 


COPY ./api/package.json /usr/app/
USER node
WORKDIR /usr/app
#entrypoint for start app
CMD ["npm", "run", "dev"]

